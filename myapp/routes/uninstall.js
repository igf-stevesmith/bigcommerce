const express = require('express'),
router = express.Router(),
BigCommerce = require('node-bigcommerce');
const bigCommerce = new BigCommerce({
    secret: '787d3b0e828678b4c3c9bfc11c8725329da1d045977ccdc5c56eefcb45de83ca',
    responseType: 'json'
});

console.log('********Uninstall - calling the router get');

router.get('/', (req, next) => {
    try {
        payLoad = req.query['signed_payload'];
        console.log(`Uninstall payload=${payLoad}`);

        const data = bigCommerce.verify(payLoad);
        console.log(data);
        res.render('uninstall', { title: 'Goodbye.  We are sorry to see you go.'});

    } catch (err) {
        next(err);
    }
});

module.exports = router;