const express = require('express'),
router = express.Router(),
BigCommerce = require('node-bigcommerce');
const bigCommerce = new BigCommerce({
    secret: '1ebd61990b65e192b8e4bb4f52be3daf6b91721d9886360500cc594baae86004',
    responseType: 'json'
});

console.log(`Load: calling get`);

router.get('/', (req, res, next) => {
    try {
        payload = req.query['signed_payload'];
        console.log(`payload= ${payload}`);
        const data = bigCommerce.verify(payload);
        console.log(data);
        res.render('welcome', { title: 'Welcome!'});
    } catch (err) {
        next(err);
        }
    });

module.exports = router;