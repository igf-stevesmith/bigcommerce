const express = require('express'),
    router = express.Router(),
    BigCommerce = require('node-bigcommerce');

const bigCommerce = new BigCommerce({
    clientId: 'tph5owgu074vrvu7wce9ap45qa86wed',
    secret: '1ebd61990b65e192b8e4bb4f52be3daf6b91721d9886360500cc594baae86004',
    callback: 'https://779df9e4afc5.ngrok.io/auth',
    responseType: 'json'
    });

console.log('auth:calling the router get');

router.get('/', (req, res, next) => {
    console.log(`req query: ${req.query}`);
    bigCommerce.authorize(req.query)
        .then(data => console.log(data))
        .then(data => res.render('auth', { title: 'Authorized!' })
        .catch(err));
        });
module.exports = router;